const mongoose = require('mongoose');
const CategorySchema = new mongoose.Schema({
  category_id: {
        type: String,
        required: [true, 'Why no category_id?'],
  },
  name: {
        type: String,
        required: [true, 'Why no name?'],
  },
  parentId: String,
  picture: String,
  count: Number,
  sort: Number
});
mongoose.model('Category', CategorySchema);

module.exports = mongoose.model('Category');
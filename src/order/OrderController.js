const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Order = require('./Order');
const Basket = require('../basket/Basket');

// CREATES A NEW Order
router.post('/', (req, res) => {
    new BaseController(Order, res).create(req.body);
});
// RETURNS ALL THE Orders IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Order, res).getAll();
});
// DELETES ALL Orders FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Order, res).deleteAll();
});

// GETS A SINGLE Order FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Order, res).get(req.params.id);
});
// UPDATE A Order FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Order, res).update(req.params.id, req.body);
});
// DELETES A Order FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Order, res).delete(req.params.id);
});

router.get('/user/:token', (req, res) => {
    Order.find({ token: req.params.token }, (err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the basket of user."});
        console.log(items);
        res.status(200).send(items);
    });  
});

module.exports = router;
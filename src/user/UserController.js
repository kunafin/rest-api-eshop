const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

router.use(bodyParser.urlencoded({ extended: true }));
const User = require('./User');

// CREATES A NEW USER
router.post('/', function (req, res) {
    new BaseController(User, res).create(req.body);
});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
     new BaseController(User, res).getAll();
});

// DELETES A APP FROM THE DATABASE
router.delete('/', function (req, res) {
    new BaseController(User, res).deleteAll();
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', function (req, res) {
    new BaseController(User, res).get(req.params.id);
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
    new BaseController(User, res).delete(req.params.id);
});

// UPDATES A SINGLE USER IN THE DATABASE
// Added VerifyToken middleware to make sure only an authenticated user can put to this route
router.put('/:id', VerifyToken, function (req, res) {
    User.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, user) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(user);
    });
});


module.exports = router;
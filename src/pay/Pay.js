const mongoose = require('mongoose');
const PaySchema = new mongoose.Schema({
  name: {
      type: String,
      required: [true, 'Why no name?']
  }
});
mongoose.model('Pay', PaySchema);

module.exports = mongoose.model('Pay');

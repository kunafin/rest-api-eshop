const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Category = require('./Category');
const Product = require(__root + 'src/product/Product');

// CREATES A NEW Category
router.post('/', (req, res) => {
    new BaseController(Category, res).create(req.body);
});

// RETURNS ALL THE Categories IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Category, res).getAll();
});

// GETS A SINGLE Category FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Category, res).get(req.params.id);
});

// DELETES ALL Categories FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Category, res).deleteAll();
});

// DELETES A CATEGORY FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Category, res).delete(req.params.id);
});

// UPDATE A CATEGORY FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Category, res).update(req.params.id, req.body);
});
// RETURN ALL SUBSECTION A CATEGORY FROM THE DATABASE
router.get('/:parentId/sections', (req, res) => {
    Category.find({ parentId: req.params.parentId }, [], {sort: {sort: 1}}, (err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the SubCategories."});
        res.status(200).send(items);
    });  
});

router.get('/:categoryId/products', (req, res) => {
	let {count, page} = req.query;
	count = Number(count) ;
	page = Number(page) - 1;
    Product.find({ categoryId: req.params.categoryId }, 
    	["name", "product_id", "price", "oldprice", "quantity", "currencyId","categoryId", "picture"], 
    	{skip: page, limit: count, sort: {sort: 1}},
    	(err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the products."});
        res.status(200).send(items);
    });  
});


module.exports = router;
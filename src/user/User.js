var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  login: {
        type: String,
      },
  email: {
        type: String,
        required: [true, 'Why no email?'],
      },
  password: {
        type: String,
        required: [true, 'Why no password?']
  },
  name: String,
  active: Boolean,
  date_create: { type: Date, default: Date.now },
  date_update: { type: Date, default: Date.now },
  group: String
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');
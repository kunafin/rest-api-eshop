/*
 * Интефейс для CRUD
 * @param {object} ModelDB, модель с интерфейсом Mongoose
 * @param {object} Res, объект роутера Express.js, возврящает результат на страницу
*/

class BaseController {
  constructor(ModelDB, Res) {
    this.ModelDB = ModelDB;
    this.Res = Res;
  }
  /*
   * @param {object} obj, данные для добавления БД
  */
  create(obj) {
    this.ModelDB.create(obj, (err, item) => {
        if (err) {
          return this.Res.status(500).send({"error" :`There was a problem adding the information to the database.` + err});
        }
        this.Res.status(200).send(item);
    });
  }
  get(id) {
    this.ModelDB.findById(id,  (err, items) =>  {
        if (err) {
          return this.Res.status(500).send({"error": `There was a problem ${this.ModelDB.modelName} finding.`});
        }
        if (!items) return this.Res.status(404).send(`No ${this.ModelDB.modelName} found.`);
        this.Res.status(200).send(items);
    });
  }
  getAll() {
    console.log();
    this.ModelDB.find({}, [], {sort: {sort: 1}}, (err, items) => {
        if (err) {
          return this.Res.status(500).send({"error": `There was a problem ${this.ModelDB.modelName} finding.`});
        }
        this.Res.status(200).send(items);
    });
  }
  /**
   * Loads all elements from the database.
   */
  getLength() {
   this.ModelDB.count({}, (err, count) => {
        if (err) {
          return this.Res.status(500).send({"error": "There was a problem counting."});
        }
        this.Res.status(200).send({count});
    });
  }

    /**
     * Updates a element with given id with given update object.
     * @param {string} id id of the element to update
     * @param {object} obj element object fields to update
     */
    update(id, obj) {
       this.ModelDB.findByIdAndUpdate(id, obj, {new: true}, (err, user) => {
          if (err) {
            return this.Res.status(500).send({"error": "There was a problem updating the user."});
          }
          this.Res.status(200).send(user);
      });
    }


  /**
   * Deletes a element with a given id from the database.
   * @param {string} id id of the object to remove
   */
  delete(id) {
    this.ModelDB.findByIdAndRemove(id, (err, item) => {
        if (err) {
          return this.Res.status(500).send({"error": "There was a problem deleting."});
        } 
        this.Res.status(200).send(`${this.ModelDB.modelName}: ${item._id} was deleted.`);
    });
  }
  /**
   * Delete all elements from the database.
   */
  deleteAll() {
    this.ModelDB.find({}, (err, items) => {
        let removeItems = items.map((item, key) => {
            this.ModelDB.findByIdAndRemove(item._id, (err) => {
              if (err) {
                return this.Res.status(500).send({"error": "There was a problem deleting." + err});
              }
            })
            return item._id;
        })

        Promise.all(removeItems).then((results) => {
            console.log('clear', results)
            this.Res.status(200).send({'clear': results});
        })
    });
    
  }
}


module.exports = BaseController;
const mongoose = require('mongoose');
const ProductSchema = new mongoose.Schema({
  product_id: {
    type: String,
    required: [true, 'Why no product_id?'],
  },
  name: {
    type: String,
    required: [true, 'Why no name?'],
  },
  vendor: String,
  vendorCode: String,
  quantity: Number,
  currencyId: {
    type: String,
    default: "RUB"
  },
  price: Number,
  oldprice: Number,
  picture: Array,
  categoryId: {
        type: String,
        required: [true, 'Why no categoryId?'],
  },
  properties: Array,
  description: String,
  sort: Number
});


mongoose.model('Product', ProductSchema);
module.exports = mongoose.model('Product');


const mongoose = require('mongoose');
const BasketSchema = new mongoose.Schema({
  product_id: {
        type: String,
        required: [true, 'Why no product_id?'],
  },
  quantity: {
        type: Number,
        min: 1,
        default: 1
  },
  date: {
        type: Date,
        default: Date.now(),
  },
  token: {
        type: String,
        required: [true, 'Why no token?'],
  },
  // user_id: {
  //       type: String,
  //       required: [true, 'Why no user_id?'],
  // }
});
mongoose.model('Basket', BasketSchema);

module.exports = mongoose.model('Basket');
const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Basket = require('./Basket');
const Product = require('../product/Product');

// CREATES A NEW Basket
router.post('/', (req, res) => {
    new BaseController(Basket, res).create(req.body);
});

// RETURNS ALL THE Baskets IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Basket, res).getAll();
});

// DELETES ALL Baskets FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Basket, res).deleteAll();
});

// GETS A SINGLE Basket FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Basket, res).get(req.params.id);
});

// UPDATE A Basket FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Basket, res).update(req.params.id, req.body);
});

// DELETES A Basket FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Basket, res).delete(req.params.id);
});

// RETURN ALL Baskets of User
router.get('/user/:token', (req, res) => {
	
    Basket.find({ token: req.params.token }, (err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the basket of user."});
        
         let productIds = items.map((item, index, arr)=>{

             return String(item.product_id);
        })

        Product.find({product_id: productIds}, ["name", "product_id", "price", "oldprice", "quantity", "currencyId","categoryId", "picture"], (err, product_items)=>{

            let newItems = items.map((item) => {

               let product = product_items.find((product)=>{
                    
                    return product.product_id == item.product_id
                })
                product = { ...item._doc, ...{product : product } };
                return product; 
            })
            res.status(200).send(newItems);
        })
        
        
    });  
});
// DElete ALL Baskets of User
router.delete('/user/:token', (req, res) => {
    
    Basket.find({ token: req.params.token }, (err, items) => {
        let removeItems = items.map((item, key) => {
            Basket.findByIdAndRemove(item._id, (err) => {
              if (err) {
                return res.status(500).send({"error": "There was a problem deleting." + err});
              }
            })
            return item._id;
        })

        Promise.all(removeItems).then((results) => {
            console.log('clear', results)
            res.status(200).send({'clear': results});
        })
        
    });  
});

module.exports = router;
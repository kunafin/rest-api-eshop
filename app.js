var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const bodyParser = require('body-parser');
const multer = require('multer');

var db = require('./db');
global.__root   = __dirname + '/';
const xml = require('./xml');
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-www-form-urlencoded");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/xml', xml.print);

app.use('/.well-known/acme-challenge', express.static(__dirname + '/.well-known/acme-challenge'));

app.get('/chat', function(req, res){
  res.sendFile(__dirname + '/chat.html');
});
app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

var UserController = require(__root + 'src/user/UserController');
app.use('/api/users', UserController);

var AuthController = require(__root + 'src/auth/AuthController');
app.use('/api/auth', AuthController);

var CategoryController = require(__root + 'src/category/CategoryController');
app.use('/api/categories', CategoryController);

var ProductController = require(__root + 'src/product/ProductController');
app.use('/api/products', ProductController);

var BasketController = require(__root + 'src/basket/BasketController');
app.use('/api/baskets', BasketController);

var FavoriteController = require(__root + 'src/favorite/FavoriteController');
app.use('/api/favorites', FavoriteController);

var OrderController = require(__root + 'src/order/OrderController');
app.use('/api/orders', OrderController);

var DeliveryController = require(__root + 'src/delivery/DeliveryController');
app.use('/api/delivery', DeliveryController);

var PayController = require(__root + 'src/pay/PayController');
app.use('/api/pay', PayController);

//принимаем файлы
app.use('/uploads', express.static(__dirname + '/uploads'));
//manual https://www.npmjs.com/package/multer

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

var uploading = multer({ storage: storage })

app.post('/uploads', uploading.single('file_app'), function (req, res) {
   res.status(200).send({success: "file uploading"});
   //обработки ошибок нет!!!
})

// io.on('connection', function(socket){
//   console.log('a user connected');
//   var room = socket.handshake['query']['r_var'];

//   socket.join(room);
//   console.log('user joined room #'+room);
//   //socket.broadcast.emit('hi');
 
//   socket.on('chat message #'+room, function(msg){
//     io.emit('chat message #'+room, msg);
//     console.log('message: ' + msg);
//   });

//   var User = require('./src/user/User');
//   var Room = require('./src/room/Room');
//   socket.on("authUser", function({name, email, password}){
//     User.create({
//         name : name,
//         email : email,
//         password : password
//     },
//     function (err, user) {
//         if (err) return res.status(500).send("There was a problem adding the information to the database." + err);
      
//         io.emit('authUser', user)
//     });
//     //io.emit('getUsers', msg);
//   });

//   socket.on("getUsers", function(){
//     User.find({}, function (err, users) {
//         if (err) return console.log("There was a problem finding the users.");
//         io.emit('getUsers', users)
//     });
//     //io.emit('getUsers', msg);
//   });
  
//   socket.on("getRoom", function(users_id){
    
//     Room.findOne({ users_id: { $all: users_id } }, function (err, room) {
//         if (err) return console.log("There was a problem finding the users.");
//         //io.emit('getUsers', users)
//         if(!room) {
//           Room.create({
//               users_id : users_id,
//           },
//           function (err, room) {
//               if (err) return res.status(500).send("There was a problem adding the information to the database." + err);
//               console.log('create room', room);
//               socket.emit("getRoom to user #"+users_id[0], room)
//               createChatRoom(room._id)
//           });
//         } else {
//           console.log('find room', room);
//           socket.emit("getRoom to user #"+users_id[0], room)
//           createChatRoom(room._id)
//         }
//     });
//     //io.emit('getUsers', msg);
//   });
//   function createChatRoom(room) {
//     socket.on('chat message #'+room, function(msg) {
//       io.emit('chat message #'+room, msg);
//       console.log('message: ' + msg);
//     });
//   }
//   // socket.on('chat message', function(msg){
//   //   io.emit('chat message', msg);
//   //   console.log('message: ' + msg);
//   // });

//   socket.on('disconnect', function(){
//       console.log('user disconnected');
//   });
// });

module.exports = http;
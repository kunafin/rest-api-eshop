const mongoose = require('mongoose');
const FavoriteSchema = new mongoose.Schema({
  product_id: {
        type: String,
        required: [true, 'Why no product_id?'],
  },
  date: {
        type: Date,
        default: Date.now(),
  },
  token: {
        type: String,
        required: [true, 'Why no token?'],
  },
  // user_id: {
  //       type: String,
  //       required: [true, 'Why no user_id?'],
  // }
});
mongoose.model('Favorite', FavoriteSchema);

module.exports = mongoose.model('Favorite');
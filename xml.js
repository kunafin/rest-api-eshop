const fs = require('fs');
const convert = require('xml-js');
const db = require('./db');

const Product = require('./src/product/Product');
const Category = require('./src/category/Category');
const Delivery = require('./src/delivery/Delivery');
const Pay = require('./src/pay/Pay');
exports.print = (err, res) => {
    

    function _textOrNull (val) {
        return !!val ? val._text : null;
    }
    function dropCollection(Collection) {
         Collection.find({}, (err, items) => {
            let removeItems = items.map((item, key) => {
                Collection.findByIdAndRemove(item._id, (err) => {
                  if (err) {
                    console.log('xml-drop', err);
                  }
                })
                return item._id;
            })

            Promise.all(removeItems).then((results) => {
                console.log('clear', results)
            })
        });
    }
    //Добавим тестовую доставку
    dropCollection(Delivery)
    Delivery.create({
        name: "Самовывоз",
        price: 0
    })
    Delivery.create({
        name: "Курьером",
        price: 500,
        free: 30000
    })
    Delivery.create({
        name: "Почта России",
        price: 250,
        free: 15000
    })
    //Добавим тестовую оплату
    dropCollection(Pay)
    Pay.create({
        name: "Наличными"
    })
    Pay.create({
        name: "Банковской картой"
    })
    Pay.create({
        name: "Яндекс.Деньги"
    })
    //очистим таблицу products
    //Product.drop();
    dropCollection(Product)


    //очистим таблицу catigories
    //Category.drop();
    dropCollection(Category)
        
    //парсим xml
    fs.readFile(__dirname + '/uploads/YML.xml', function(err, data) {
        var start, stop, i = 0; //метка времения
        start = (new Date()).getTime();

        //преобразуем xml в json
        let str_res = convert.xml2json(data, {compact: true, spaces: 4});
        let json_res = JSON.parse(str_res);


        //написать элегантный поиск по ключу,
        //обязательно посмотреть на время исполнения,
        //проверив на тяжелом файле
        let category = json_res.yml_catalog.shop.categories.category;
        let offers = json_res.yml_catalog.shop.offers.offer;


        


        offers.forEach(function(item, index) {
            let params = null;

            if(!!item.param) {

                   if(Array.isArray(item.param)) {
                        //массив
                        params =  item.param.map(function(prop) {
                            let props = {
                                name : prop._attributes.name,
                                value: prop._text
                            }
                            return props;
                        });
                   } else {
                       //не массив
                        params = new Array;
                        params[0] = item.param;
                   }
            }

           let picture = null;
           if(!!item.picture) {

                if(Array.isArray(item.picture)) {
                    //массив
                    picture =  item.picture.map(function(prop) {
                        let props = prop._text
                        return props;
                     });
               } else {
                    picture = new Array;
                    picture.push(item.picture._text);
               }
            }

            let el = {
                product_id: item._attributes.id,
                name: _textOrNull(item.model),
                price: _textOrNull(item.price),
                oldprice: _textOrNull(item.oldprice),
                currencyId: _textOrNull(item.currencyId),
                categoryId: _textOrNull(item.categoryId),
                vendor: _textOrNull(item.vendor),
                vendorCode: _textOrNull(item.vendorCode),
                description: _textOrNull(item.description),
                quantity: _textOrNull(item.quantity),
                properties: params,
                picture: picture,
                sort: index,
            };

           // запись товара
           Product.create(el);

        });


        //заполняем таблицу categories {


            category.forEach(function(item, index) {
                let category_id = item._attributes.id;

                let count_product = offers.filter((item, index)=>{
                    return _textOrNull(item.categoryId) == category_id;
                }).length;

                let el = {
                    category_id: category_id,
                    name: item._text,
                    parentId: item._attributes.parentId,
                    picture: item._attributes.picture,
                    count: count_product,
                    sort: index
                };

                //запись категорий
                //Category.insert(el);
                Category.create(el);


            });
        //}

        //индекс для Поиска https://docs.mongodb.com/manual/text-search/
        Product.createIndexes( { name: "text", vendor: "text", description: "text" } );

        stop = (new Date()).getTime();
        console.log('Время выполнения = ', stop - start);

        //console.log(category);
        res.send( json_res );
    });
}



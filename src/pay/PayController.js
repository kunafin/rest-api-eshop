const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Pay = require('./Pay');

// CREATES A NEW Pay
router.post('/', (req, res) => {
    new BaseController(Pay, res).create(req.body);
});
// RETURNS ALL THE Pay IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Pay, res).getAll();
});
// DELETES ALL Pay FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Pay, res).deleteAll();
});

// GETS A SINGLE Pay FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Pay, res).get(req.params.id);
});
// UPDATE A Pay FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Pay, res).update(req.params.id, req.body);
});
// DELETES A Pay FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Pay, res).delete(req.params.id);
});


module.exports = router;
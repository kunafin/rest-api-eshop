const mongoose = require('mongoose');
const DeliverySchema = new mongoose.Schema({
  name: {
      type: String,
      required: [true, 'Why no name?']
  },
  price: {
      type: Number,
      required: [true, 'Why no price?']
  },
  free: Number
});
mongoose.model('Delivery', DeliverySchema);

module.exports = mongoose.model('Delivery');

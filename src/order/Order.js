const mongoose = require('mongoose');
const OrderSchema = new mongoose.Schema({
  date: {
        type: Date,
        default: Date.now(),
  },
  token: {
        type: String,
        required: [true, 'Why no token?'],
  },
  status: {
        type: String,
        default: "active", // active | canceled | success
  }, 
  delivery: {
    type: Map,
    of: String
  },
  pay: {
    name: String,
    status: {
        type: String,
        default: "no paid", // paid
    }
  },
  summ: {
        type: Number,
        required: [true, 'Why no summ?'],
        min: 1
  },
  user: {
    type: Map,
    of: String
  },
  basket: {
    type: Map
  }
  
  // user_id: {
  //       type: String,
  //       required: [true, 'Why no user_id?'],
  // }
});
mongoose.model('Order', OrderSchema);

module.exports = mongoose.model('Order');

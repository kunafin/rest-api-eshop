const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Favorite = require('./Favorite');
const Product = require('../product/Product');

// CREATES A NEW Favorite
router.post('/', (req, res) => {
    new BaseController(Favorite, res).create(req.body);
});

// RETURNS ALL THE Favorites IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Favorite, res).getAll();
});

// DELETES ALL Favorites FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Favorite, res).deleteAll();
});

// GETS A SINGLE Favorite FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Favorite, res).get(req.params.id);
});

// UPDATE A Favorite FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Favorite, res).update(req.params.id, req.body);
});

// DELETES A Favorite FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Favorite, res).delete(req.params.id);
});

// RETURN ALL Favorites of User
router.get('/user/:token', (req, res) => {
	
    Favorite.find({ token: req.params.token }, (err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the favorite of user."});
        
         let productIds = items.map((item, index, arr)=>{

             return String(item.product_id);
        })

        Product.find({product_id: productIds}, (err, product_items)=>{

            let newItems = items.map((item) => {

               let product = product_items.find((product)=>{
                    
                    return product.product_id == item.product_id
                })
                product = { ...item._doc, ...{product : product } };
                return product; 
            })
            res.status(200).send(newItems);
        })
        
        
    });  
});

module.exports = router;
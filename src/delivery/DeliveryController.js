const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Delivery = require('./Delivery');

// CREATES A NEW Delivery
router.post('/', (req, res) => {
    new BaseController(Delivery, res).create(req.body);
});
// RETURNS ALL THE Deliveris IN THE DATABASE
router.get('/', (req, res) => {
    new BaseController(Delivery, res).getAll();
});
// DELETES ALL Deliveris FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Delivery, res).deleteAll();
});

// GETS A SINGLE Delivery FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Delivery, res).get(req.params.id);
});
// UPDATE A Delivery FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Delivery, res).update(req.params.id, req.body);
});
// DELETES A Delivery FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Delivery, res).delete(req.params.id);
});



module.exports = router;
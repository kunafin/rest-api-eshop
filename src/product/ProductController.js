const express = require('express');
const router = express.Router();

const VerifyToken = require(__root + 'src/auth/VerifyToken');
const BaseController = require(__root + 'src/BaseController');

const Product = require('./Product');

// CREATES A NEW Product
router.post('/', (req, res) => {
    new BaseController(Product, res).create(req.body);
});

// RETURNS ALL THE Products IN THE DATABASE
router.get('/', (req, res) => {
    let {count, page, search} = req.query;
    let find = {};
	count = Number(count) ;
	page = Number(page) - 1;
	
	if( search ) {
    	find = { $text: { $search: `\"${search}\"` } }
  	}
    Product.find(find, 
    	["name", "product_id", "price", "oldprice", "quantity", "currencyId","categoryId", "picture"], 
    	{skip: page, limit: count, sort: {sort: 1}},
    	(err, items) => {
        if (err) return res.status(500).send({"error": "There was a problem finding the products."});
        res.status(200).send(items);
    });  
});

// GETS A SINGLE Product FROM THE DATABASE
router.get('/:id',  (req, res) => {
    new BaseController(Product, res).get(req.params.id);
});

// DELETES ALL Products FROM THE DATABASE
router.delete('/',  (req, res) => {
    new BaseController(Product, res).deleteAll();
});

// DELETES A Product FROM THE DATABASE
router.delete('/:id', (req, res) => {
    new BaseController(Product, res).delete(req.params.id);
});

// UPDATE A Product FROM THE DATABASE
router.put('/:id', (req, res) => {
    new BaseController(Product, res).update(req.params.id, req.body);
});

module.exports = router;